## Interface: 110000
## Title: Tooltip Enhanced Revived
## Author: braggerXYZ
## Version: 1.0.0
## Notes: A lightweight tooltip addon that preserves the default style.
## License: MIT/X Consortium License

Config.lua
SpellID.lua
TooltipAnchor.lua
TooltipEnhancedRevived.lua
