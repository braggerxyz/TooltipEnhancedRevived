local _, db = ...
local c = db.config

local function colorizeTooltip(tooltip, db)
    if not db.unitIsPlayer or (not c.enableClassColor and not c.enableGuildColor) then return end

    for i = 1, tooltip:NumLines() do
        local line = _G[tooltip:GetName() .. 'TextLeft' .. i]
        local lineText = line:GetText()

        -- Name line
        if i == 1 and c.enableClassColor then
            line:SetText( '|c' .. db.unitClassColor.colorStr .. lineText .. '|r' )
        -- Guild line
        elseif i == 2 and db.unitGuildInfo[1] and c.enableGuildColor then
            line:SetText('|c' .. c.guildColor:GenerateHexColor() .. lineText .. ' <' .. db.unitGuildInfo[2] .. '>' .. '|r' )
        -- Faction line, buggy as shit, needs fixing
        elseif (lineText == FACTION_HORDE or lineText == FACTION_ALLIANCE) and c.enableClassColor then
            --if db.unitFaction ~= select(2, UnitFactionGroup('player')) and c.enableClassColor then
            if lineText == FACTION_HORDE then
                line:SetTextColor(1, 0, 0)
            elseif lineText == FACTION_ALLIANCE then
                line:SetTextColor(0, 0.5, 1)
            end
        end
    end
    tooltip:Show()
end

local function onTooltipSetUnit(self)
    if IsMouseButtonDown('LeftButton') or IsMouseButtonDown('RightButton') then
        return self:Hide()
    end

    local unit
    if not unit and UnitExists('mouseover') then
        unit = 'mouseover'
    end
    
    if unit then
        db.unitType = UnitGUID(unit):match('^(.-)%-'):lower()
        db.unitIsPlayer = db.unitType == 'player'
        db.unitIsPet = db.unitType == 'pet'
        db.unitIsMine = UnitIsUnit(unit, 'player') or UnitIsUnit(unit, 'pet') -- me or my pet
        db.unitInGroup = UnitPlayerOrPetInRaid(unit) or UnitPlayerOrPetInParty(unit)
        db.unitFaction = select(2, UnitFactionGroup(unit))
        db.unitReactionColor = { GameTooltip_UnitColor(unit) } -- { r, g, b }
        db.unitGuildInfo = db.unitIsPlayer and { GetGuildInfo(unit) } -- { name, rankName, rankIndex, realmName }
        db.unitClassColor = db.unitIsPlayer and RAID_CLASS_COLORS[ select(2, UnitClass(unit)) ]
        colorizeTooltip(self, db)
    end
end

TooltipDataProcessor.AddTooltipPostCall(Enum.TooltipDataType.Unit, onTooltipSetUnit)
