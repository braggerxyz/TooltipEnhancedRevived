local _, db = ...
local c = db.config

if not c.attachToCursor then
    return
end

local function setDefaultAnchor(self, parent)
    self:SetOwner(parent, 'ANCHOR_NONE')
    self:ClearAllPoints()

    self:SetPoint('BOTTOMRIGHT', UIParent, 'BOTTOMRIGHT', -CONTAINER_OFFSET_X - 13, CONTAINER_OFFSET_Y)

    if c.detachInCombat and not InCombatLockdown() then
        self:SetOwner(parent, 'ANCHOR_CURSOR_RIGHT', c.offsetX, c.offsetY)
    end
end
hooksecurefunc('GameTooltip_SetDefaultAnchor', setDefaultAnchor)