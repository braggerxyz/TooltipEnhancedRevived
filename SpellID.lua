local _, db = ...
local c = db.config

if not c.showSpellID then
    return -- ignore all below
end

local spellIDTextColor = db.config.spellIDTextColor:GenerateHexColor()

local function addLine(tooltip, id)
    if not id then return end

    tooltip:AddLine(" ", 1, 1, 1)
    tooltip:AddLine('|c' .. spellIDTextColor .. db.config.spellIDText .. '|r ' .. id, 1, 1, 1)
    tooltip:Show() -- update frame size
end

-- BuffFrame, UnitFrame
local function setUnitAura(self, unit, index, filter)
    local id = select(10, C_UnitAuras.GetAuraDataByIndex(unit, index, filter))

    addLine(self, id)
end

-- SpellBookFrame, PlayerTalentFrame, ...
local function onTooltipSetSpell(self)
    local id = select(2, self:GetSpell())

    -- A duplicate line appears in PlayerTalentFrame
    for i = self:NumLines(), 3, -1 do
        local lineText = _G[self:GetName() .. 'TextLeft' .. i]:GetText()

        -- If "Spell ID" line is found
        if string.match(lineText, db.config.spellIDText) then
            return -- do not add SpellID line a second time
        end
    end
    addLine(self, id)
end

hooksecurefunc(GameTooltip, 'SetUnitAura', setUnitAura)
hooksecurefunc(GameTooltip, 'SetUnitBuff', setUnitAura)
hooksecurefunc(GameTooltip, 'SetUnitDebuff', setUnitAura)
TooltipDataProcessor.AddTooltipPostCall(Enum.TooltipDataType.Spell, onTooltipSetSpell)