local _, db = ...

db.config = {
    attachToCursor    = true,
    -- Possible positions: ANCHOR_CURSOR_RIGHT, ANCHOR_CURSOR_LEFT, ANCHOR_CURSOR
    mouseAnchorPos    = 'ANCHOR_CURSOR_RIGHT',
    detachInCombat    = true,
    offsetX           = 16,
    offsetY           = 10,

    enableClassColor  = true,
    enableGuildColor  = true,
    guildColor        = CreateColor(0.251, 1, 0.251),
    hideUnitTitle     = false,
    showUnitHealth    = true,
    showToT           = true,
    ToTText           = 'Target: ',
    ToTYouText        = '>> YOU <<',

    showSpellID       = true,
    spellIDText       = 'Spell ID:',
    spellIDTextColor  = CreateColor(1, 0.50196, 0), -- LEGENDARY_ORANGE_COLOR
}